from psd_tools import PSDImage
from PIL import Image
import re
from datetime import date

def set_layer_state(layer, language):
    if layer.is_group():
        for item in layer:
            set_layer_state(item, language)
    else:
        language_regex = r'.*_(.{2})'
        regex_result = re.search(language_regex, layer.name)
        if regex_result is not None:
            if regex_result[1] != language:
                layer.visible = False
            else:
                layer.visible = True

def save_psd_to_pdf(psd, language):
    set_layer_state(psd, language)
    rgba = psd.compose(force=True)
    rgb = Image.new('RGB', rgba.size, (255, 255, 255))
    rgb.paste(rgba, mask=rgba.split()[3])
    rgb.save(f"francois-guergadic-{date.today()}-{language}.pdf", 'PDF', resoultion=100.0)

if __name__ == "__main__":
    psd = PSDImage.open("cv.psd")
    save_psd_to_pdf(psd, 'fr')
    save_psd_to_pdf(psd, 'en')