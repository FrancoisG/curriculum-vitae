import argparse
import dropbox
from dropbox.files import WriteMode
from datetime import date


class TransferData:
    def __init__(self, access_token):
        self.access_token = access_token

    def upload_file(self, file_from, file_to):
        """
        Upload a file to Dropbox using API v2
        """
        dbx = dropbox.Dropbox(self.access_token)

        with open(file_from, "rb") as f:
            dbx.files_upload(f.read(), file_to, mode=WriteMode("overwrite"))


def transfer_file(language, token):
    filename = f"francois-guergadic-{date.today()}-{language}.pdf"
    transferData = TransferData(token)
    file_to = f"/Administratif/Resume/{filename}"
    transferData.upload_file(filename, file_to)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Copy file to dropbox.")
    parser.add_argument(
        "--token", help="Dropbox Token", type=str, required=True,
    )
    args = parser.parse_args()

    transfer_file('fr', args.token)
    transfer_file('en', args.token)
